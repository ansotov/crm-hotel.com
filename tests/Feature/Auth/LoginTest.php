<?php

namespace Tests\Feature\Auth;

use App\Http\Controllers\ArticlesController;
use App\Models\User;
use Mockery;
use Tests\TestCase;

class LoginTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->mock = Mockery::mock(ArticlesController::class);

        $this->mock
            ->shouldReceive('create')
            ->once();

        $this->app->instance('ArticlesController', $this->mock);
        $test = $this->mock->create();
    }
}
