/**
 * Data to window
 * @param url
 */
function dataToWindow(url) {

    var wrapper = $('#window-wrapper');
    wrapper.html('');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            wrapper.html(data);

            $('.close-box-collapse, .click-closed').on('click', function () {
                $('body').removeClass('box-collapse-open').addClass('box-collapse-closed');
                $('.menu-list ul').slideUp(700);
            });

            sendForm();
        },
        error: function (data) {
            console.log(data);
        }
    });
}

$('.logout').on('click', function (e) {

    e.preventDefault();

    var url = $(this).attr('href');

    $.confirm({
        template: '<div class="jconfirm"><div class="jconfirm-bg jconfirm-bg-h"></div><div class="jconfirm-scrollpane"><div class="jconfirm-row"><div class="jconfirm-cell"><div class="jconfirm-holder"><div class="jc-bs3-container"><div class="jc-bs3-row"><div class="jconfirm-box-container jconfirm-animated"><div class="jconfirm-box" role="dialog" aria-labelledby="labelled" tabindex="-1"><div class="jconfirm-closeIcon">&times;</div><div class="jconfirm-title-c"><span class="jconfirm-icon-c"></span><span class="jconfirm-title"></span></div><div class="jconfirm-buttons"></div><div class="jconfirm-clear"></div></div></div></div></div></div></div></div></div></div>',
        title: 'Вы уверены?',
        content: false,
        buttons: {
            confirm: {
                text: 'Да',
                btnClass: 'btn-green',
                action: function () {
                    document.getElementById('logout-form').submit();
                }
            },
            cancel: {
                text: 'Нет'
            }
        }
    });
});

function sendForm() {
    $('form.ajax-send').on('submit', function (event) {

        // ansotov останавливаем переход по ссылке
        event.preventDefault();

        //ansotov получаем url
        var href = $(this).attr('action');
        var method = $(this).attr('method');

        //ansotov формируем данные из формы
        var formData = $(this).serializeToObject();

        $.ajax({
            url: href,
            method: method,
            data: formData,
            success: function (data) {
                window.location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var data = $.parseJSON(xhr.responseText);

                var items = [];

                $.each(data.errors, function (key, val) {
                    items.push('<li id="' + key + '">' + val + '</li>');
                });

                $('.formErrors').html('<ul>' + items + '</ul>');
            }
        });
    });
}

$.fn.serializeToObject = function () {
    var object = {}; //создаем объект
    var a = this.serializeArray(); //сериализируем в массив
    $.each(a, function () { //проходимся по массиву и добавляем параметр name как имя свойства объекта и value как его значение
        if (object[this.name] !== undefined) { //не забываем проверить данные
            if (!object[this.name].push) {
                object[this.name] = [object[this.name]];
            }
            object[this.name].push(this.value || '');
        } else {
            object[this.name] = this.value || '';
        }
    });
    return object;
};
