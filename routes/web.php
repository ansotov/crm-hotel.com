<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Auth
 */
Auth::routes();
Route::post('/code-login', 'Auth\LoginController@code')->name('auth.code');

Route::get('/', 'MainController@index')->name('main');

Route::group(
    ['middleware' => 'auth'],
    function () {
        // Services
        Route::get('/services', 'ServicesController@index')->name('services');
        Route::get('/services/{slug}', 'ServicesController@item')->name('services.item');

        // Orders
        Route::get('/orders', 'OrdersController@index')->name('orders');
        Route::get('/orders/{slug}', 'OrdersController@item')->name('orders.item');
        Route::post('/order', 'OrdersController@create')->name('orders.create');

        // Offers
        Route::get('/offers', 'OffersController@index')->name('offers');
        Route::get('/offers/{slug}', 'OffersController@item')->name('offers.item');

        // Articles
        Route::get('/articles', 'ArticlesController@index')->name('articles');
        Route::get('/articles/{slug}', 'ArticlesController@item')->name('articles.item');
        Route::get('/contacts', 'ArticlesController@contacts')->name('contacts');

        // Profile
        Route::get('/profile', 'UserController@editProfile')->name('profile.edit');
        Route::patch('/profile', 'UserController@updateProfile')->name('profile.update');

        Route::get('/set-lang/{lang}', 'MainController@lang')->name('lang');
    }
);
