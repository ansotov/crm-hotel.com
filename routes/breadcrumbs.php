<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(__('menu.home'), route('main'));
});

// Home > Services
Breadcrumbs::for('services', function ($trail) {
    $trail->push(__('menu.services'), route('services'));
});

// Home > Services > [Item]
Breadcrumbs::for('services.item', function ($trail, $item) {
    $trail->parent('services');
    $trail->push($item->data->title, route('services.item', $item->slug));
});

// Home > Offers
Breadcrumbs::for('offers', function ($trail) {
    $trail->push(__('menu.offers'), route('offers'));
});

// Home > Offers > [Item]
Breadcrumbs::for('offers.item', function ($trail, $item) {
    $trail->parent('offers');
    $trail->push($item->data->title, route('offers.item', $item->slug));
});

// Home > Articles
Breadcrumbs::for('articles', function ($trail) {
    $trail->push(__('menu.articles'), route('articles'));
});

// Home > Articles > [Item]
Breadcrumbs::for('articles.item', function ($trail, $item) {
    $trail->parent('articles');
    $trail->push($item->data->title, route('articles.item', $item->slug));
});

// Home > Orders
Breadcrumbs::for('orders', function ($trail) {
    $trail->push(__('menu.my-orders'), route('orders'));
});

// Home > Contacts
Breadcrumbs::for('contacts', function ($trail) {
    $trail->push(__('menu.contacts'), route('contacts'));
});
