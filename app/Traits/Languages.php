<?php

namespace App\Traits;

use App\Models\Hotels\HotelLanguage;
use App\Models\Language;

trait Languages
{
    /**
     * @return \App\Models\Language[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function languages()
    {
        return Language::all();
    }

    /**
     * @return \App\Models\Language[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function hotelLanguages($hotelId)
    {
        return HotelLanguage::whereHotelId($hotelId)->get();
    }
}
