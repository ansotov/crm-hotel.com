<?php

namespace App\Traits\Logs;

trait Data
{
    protected function dataArray($object, $class)
    {
        return [
            'class'   => $class,
            'table'   => $object->getTable(),
            'user_id' => auth()->user()->id,
            'data'    => $object,
        ];
    }
}
