<?php

namespace App\Widgets;

use App\Http\Controllers\Controller;
use Arrilot\Widgets\AbstractWidget;

class Languages extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    private $items, $currentLang;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $this->items       = resolve(Controller::class)->mainLanguages();
        $this->currentLang = resolve(Controller::class)->currentLanguage();

        return view('widgets.languages', [
            'config'      => $this->config,
            'items'       => $this->items,
            'currentLang' => $this->currentLang,
        ]);
    }
}
