<?php

namespace App\Observers\Hotels;

use App\Models\Rooms\Room;
use App\Traits\Logs\Data;
use Illuminate\Support\Facades\Log;

class RoomObserver
{
    use Data;

    /**
     * Handle the room "creating" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function creating(Room $room)
    {
        //
    }


    /**
     * Handle the room "created" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function created(Room $room)
    {
        Log::info('Created', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "updated" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function updated(Room $room)
    {
        Log::info('Updated', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "deleted" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function deleting(Room $room)
    {
        dd(333);
    }

    /**
     * Handle the room "deleted" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function deleted(Room $room)
    {
        dd(444);
        Log::info('Deleted', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "restored" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function restored(Room $room)
    {
        dd(555);
        Log::info('Restored', $this->dataArray($room, __CLASS__));
    }

    /**
     * Handle the room "force deleted" event.
     *
     * @param \App\Models\Rooms\Room $room
     *
     * @return void
     */
    public function forceDeleted(Room $room)
    {
        Log::info('Force deleted', $this->dataArray($room, __CLASS__));
    }
}
