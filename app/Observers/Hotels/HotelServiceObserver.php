<?php

namespace App\Observers\Hotels;

use App\Models\Hotels\HotelService;
use App\Models\Logs\Event;
use App\Models\Logs\EventType;
use App\Traits\Logs\Data;
use Illuminate\Support\Facades\Log;

class HotelServiceObserver
{
    use Data;

    /**
     * Handle the hotel service "created" event.
     *
     * @param \App\Models\Hotels\HotelService $hotelService
     *
     * @return void
     */
    public function created(HotelService $hotelService)
    {
        Log::info('Created', $this->dataArray($hotelService, __CLASS__));
        $event = new Event([
            'type_id'   => EventType::where(['const' => 'hotel_service_added'])->first()->id,
            'user_id'   => auth()->user()->id,
            'object_id' => $hotelService->id,
        ]);
        $event->save();
    }

    /**
     * Handle the hotel service "updated" event.
     *
     * @param \App\Models\Hotels\HotelService $hotelService
     *
     * @return void
     */
    public function updated(HotelService $hotelService)
    {
        Log::info('Updated', $this->dataArray($hotelService, __CLASS__));
    }

    /**
     * Handle the hotel service "deleted" event.
     *
     * @param \App\Models\Hotels\HotelService $hotelService
     *
     * @return void
     */
    public function deleted(HotelService $hotelService)
    {
        Log::info('Deleted', $this->dataArray($hotelService, __CLASS__));
    }

    /**
     * Handle the hotel service "restored" event.
     *
     * @param \App\Models\Hotels\HotelService $hotelService
     *
     * @return void
     */
    public function restored(HotelService $hotelService)
    {
        Log::info('Restored', $this->dataArray($hotelService, __CLASS__));
    }

    /**
     * Handle the hotel service "force deleted" event.
     *
     * @param \App\Models\Hotels\HotelService $hotelService
     *
     * @return void
     */
    public function forceDeleted(HotelService $hotelService)
    {
        Log::info('Force deleted', $this->dataArray($hotelService, __CLASS__));
    }
}
