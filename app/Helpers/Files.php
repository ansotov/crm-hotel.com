<?php
/**
 * Created by site-town.com
 * User: ansotov
 * Date: 2020-05-13
 * Time: 17:55
 */

if ( ! function_exists('frontImages')) {
    /**
     * @param $image
     *
     * @return string
     */
    function frontImages($image)
    {
        return config('app.static_url') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $image;
    }
}

if ( ! function_exists('frontHotelImages')) {
    /**
     * @param $image
     * @param $sectionType
     * @param $imageType
     *
     * @return string
     */
    function frontHotelImages($image, $sectionType, $imageType)
    {
        return config('app.static_url') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'hotels' . DIRECTORY_SEPARATOR . md5(auth()->user()->hotel_id) . DIRECTORY_SEPARATOR . $sectionType . DIRECTORY_SEPARATOR . $imageType . '_' . $image;
    }
}

if ( ! function_exists('uniqueStr')) {
    /**
     * @return string
     */
    function uniqueStr()
    {
        return md5(time() . rand(0, 100000000));
    }
}

if ( ! function_exists('offerImage')) {
    /**
     * @param $image
     * @param $sectionType
     * @param $imageType
     *
     * @return string
     */
    function offerImage($imageUrl)
    {
        return file_exists($imageUrl) ? config('app.static_url') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . $imageUrl : config('app.static_url') . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . config('app.default_image');
    }
}
