<?php

namespace App\Http\Controllers;

use App\Models\Hotels\HotelService;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->items = HotelService::active()->hotelItems();

        return view('services.items', [
            'items' => $this->items,
        ]);
    }

    public function item(Request $request, $slug)
    {
        $this->item = HotelService::active()->where(['slug' => $slug])->first();

        return view('services.item', [
            'item' => $this->item,
        ]);
    }
}
