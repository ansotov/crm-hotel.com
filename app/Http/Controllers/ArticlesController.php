<?php

namespace App\Http\Controllers;

use App\Models\Articles\Article;
use App\Models\Articles\ContactArticle;

class ArticlesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->items = Article::where(function ($query) {
            $query->active();
            $query->hotelArticles();
        })
            ->orderBy('id', 'DESC')
            ->paginate(setting('site.perpage'));

        return view('articles.items', [
            'items' => $this->items,
        ]);
    }

    /**
     * @param $slug
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function item($slug)
    {
        $this->item = Article::where(function ($query) use ($slug) {
            $query->active();
            $query->slug($slug);
        })->first();

        return view('articles.item', [
            'item' => $this->item,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts()
    {
        $this->item = ContactArticle::where(function ($query) {
            $query->hotelItem();
        })->first();

        return view('articles.contacts', [
            'item' => $this->item,
        ]);
    }
}
