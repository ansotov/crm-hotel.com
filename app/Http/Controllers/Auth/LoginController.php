<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Hotels\HotelGuest;
use App\Models\Hotels\HotelGuestCode;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Auth by code
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function code(Request $request)
    {
        $request->validate([
            'code' => 'required|min:6',
        ]);

        $issetCode = HotelGuestCode::where(['code' => trim($request->code)])->first();
        if ($issetCode && $issetCode->guest) {

            $booked = HotelGuest::where(['user_id' => $issetCode->guest->hotelUser->id])
                ->where(function ($query) {
                    $query->where('start_at', '<=', date('Y-m-d H:i:s'));
                    $query->where('end_at', '>=', date('Y-m-d H:i:s'));
                })
                ->first();

            if ($booked) {
                $user = $issetCode->guest->hotelUser->user;

                // Set hotel
                $user->hotel_id = $booked->room->hotel_id;
                $user->room_id = $booked->room->id;
                $user->save();

                Auth::login($user);

                return back()->with(['success' => __('status.welcome')]);
            } else {
                goto ext;
            }
        } else {
            ext:
            return back()->withErrors(__('validation.code-dont-isset'));
        }
    }
}
