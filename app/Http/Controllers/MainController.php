<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class MainController extends Controller
{

	/**
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
    public function index()
    {
        if (auth()->check()) {
            return Redirect::route('services');
        } else {
            return view('main');
        }
    }

	/**
	 * @param                          $lang
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function lang($lang): RedirectResponse
	{
        $this->availableLanguageIds();

        if (in_array($lang, $this->availableLanguages)) {
            $lang          = Language::where(['const' => $lang])->first();
            $item          = auth()->user();
            $item->lang_id = $lang->id;

            $item->save();
        }

        return Redirect::back();
    }
}
