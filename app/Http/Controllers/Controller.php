<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Language;
use App\Traits\Languages;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use TCG\Voyager\Models\Role;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Languages;

    protected $item, $items, $availableLanguages;

    /**
     * @return mixed
     */
    public function mainLanguages()
    {
        return Language::where(['active' => true, 'required' => true])->get();
    }

    /**
     * @return mixed
     */
    public function currentLanguage()
    {
        return Language::where(['const' => app()->getLocale()])->first();
    }

    /**
     * @param $const
     *
     * @return mixed
     */
    protected function role($const)
    {
        return Role::where(['name' => $const])->first();
    }

    /**
     *
     */
    protected function availableLanguageIds()
    {
        foreach ($this->mainLanguages() as $k => $v) {
            $this->availableLanguages[] = strtolower($v->const);
        }
    }

    /**
     * @return mixed
     */
    public function mainCurrencies()
    {
        return Currency::where(['active' => true, 'required' => true])->get();
    }
}
