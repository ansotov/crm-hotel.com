<?php

namespace App\Http\Controllers;

use App\Models\Offers\Offer;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->items = Offer::where(function ($query) {
            $query->active();
            $query->hotelItems();
        })
            ->orderBy('id', 'DESC')
            ->paginate(setting('site.perpage'));

        return view('offers.items', [
            'items' => $this->items,
        ]);
    }

    public function item(Request $request, $slug)
    {
        $this->item = Offer::where(function ($query) use ($slug) {
            $query->active();
            $query->slug($slug);
        })->first();

        return view('offers.item', [
            'item' => $this->item,
        ]);
    }
}
