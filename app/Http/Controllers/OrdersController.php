<?php

namespace App\Http\Controllers;

use App\Models\Hotels\HotelService;
use App\Models\Orders\Order;
use App\Models\Orders\OrderProduct;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    /**
     * @param \App\Models\Orders\Order $order
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(
        Order $order
    ) {
        $this->items = $order->hotelUser()->paginate(setting('site.perpage'));

        return view('orders.items',
            [
                'items' => $this->items,
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request        $request
     * @param \App\Models\Orders\Order        $order
     * @param \App\Models\Orders\OrderProduct $orderProduct
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(
        Request $request,
        Order $order,
        OrderProduct $orderProduct
    ) {
        if ($service = HotelService::active()->where(['id' => $request->service_id])->first()) {
            try {
                $order->slug      = uniqueStr();
                $order->hotel_id  = auth()->user()->hotel_id;
                $order->room_id   = auth()->user()->room_id;
                $order->user_id   = auth()->user()->id;
                $order->status_id = auth()->user()->hotel->mainStatus()->id;
                $order->comment   = $request->comment;

                $order->save();

                $order->update(
                    [
                        'slug' => $order->hotel_id . '-' . $order->user_id . $order->id
                    ]
                );

                $orderProduct->order_id    = $order->id;
                $orderProduct->service_id  = $service->id;
                $orderProduct->price       = $service->price;
                $orderProduct->currency_id = $service->currency->id;
                $orderProduct->qty         = 1;

                $orderProduct->save();
            } catch (\Exception $exception) {
                dd($exception->getMessage());
            }

            $message = ['success' => __('orders.add-success')];
        } else {
            $message = ['error' => __('orders.add-error')];
        }

        return redirect()->back()->with($message);
    }
}
