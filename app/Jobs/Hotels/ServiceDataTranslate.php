<?php

namespace App\Jobs\Hotels;

use App\Models\Hotels\HotelService;
use App\Models\Hotels\HotelServiceData;
use App\Traits\Languages;
use App\Traits\Logs\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Stichoza\GoogleTranslate\GoogleTranslate;

class ServiceDataTranslate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Languages, Data;

    private $request;
    private $item;
    private $fromLang;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(HotelService $item, $request, $fromLang)
    {
        $this->request  = $request;
        $this->item     = $item;
        $this->fromLang = $fromLang;
    }

    /**
     * @throws \ErrorException
     */
    public function handle()
    {
        foreach ($this->hotelLanguages() as $k => $v) {

            sleep(30);

            HotelServiceData::updateOrCreate(
                [
                    'service_id' => $this->item->id,
                    'lang_id'    => $v->lang_id,
                ],
                [
                    'title'       => GoogleTranslate::trans($this->request['title'], $v->language->const, $this->fromLang),
                    'description' => isset($this->request['description']) ? GoogleTranslate::trans($this->request['description'], $v->language->const, $this->fromLang) : false,
                ]
            );
        }

        // Log data
        Log::info('Queue finished ', $this->dataArray($this->item, __CLASS__));
    }
}
