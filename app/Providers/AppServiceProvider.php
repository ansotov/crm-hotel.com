<?php

namespace App\Providers;

use App\Models\Hotels\HotelService;
use App\Models\News\NewItem;
use App\Models\Rooms\Room;
use App\Observers\Hotels\HotelServiceObserver;
use App\Observers\Hotels\NewItemObserver;
use App\Observers\Hotels\RoomObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Observers
        Room::observe(RoomObserver::class);
        HotelService::observe(HotelServiceObserver::class);
        NewItem::observe(NewItemObserver::class);
    }
}
