<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $connection = 'mysql_logs';

    protected $fillable = [
        'type_id',
        'user_id',
        'object_id',
    ];
}
