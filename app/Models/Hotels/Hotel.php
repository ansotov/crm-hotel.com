<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Hotel extends Model
{

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Hotels\HotelData', 'id', 'hotel_id')->where(['lang_id' => Auth::user()->lang_id]);
    }

    /**
     * Guests
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guests()
    {
        return $this->belongsTo('App\Models\User', 'id', 'hotel_id');
    }

    /**
     * Languages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function languages()
    {
        return $this->belongsTo('App\Models\Hotels\HotelLanguage', 'id', 'hotel_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\BelongsTo|object|null
     */
    public function mainStatus()
    {
        return $this->statuses()->where(['main' => true])->first();
    }

    /**
     * Statuses
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statuses()
    {
        return $this->belongsTo('App\Models\Hotels\HotelOrderStatus', 'id', 'hotel_id');
    }
}
