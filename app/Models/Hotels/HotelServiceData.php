<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

class HotelServiceData extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'service_id',
        'lang_id',
        'title',
        'description',
    ];
}
