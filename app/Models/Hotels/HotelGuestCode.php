<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

class HotelGuestCode extends Model
{
    protected $fillable = [
        'guest_id',
        'code',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guest()
    {
        return $this->belongsTo('App\Models\Hotels\HotelGuest', 'guest_id', 'id');
    }

}
