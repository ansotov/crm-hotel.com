<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelGuest extends Model
{
    use SoftDeletes;

    protected $dates = ['start_at', 'end_at', 'created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'room_id',
        'user_id',
        'start_at',
        'end_at',
        'comment',
    ];

    /**
     * Room
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function room()
    {
        return $this->belongsTo('App\Models\Rooms\Room', 'room_id', 'id');
    }

    /**
     * HotelUser
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotelUser()
    {
        return $this->belongsTo('App\Models\Hotels\HotelUser', 'user_id', 'id');
    }

    /**
     * Scope a query to only include hotel guests
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelGuests($query)
    {
        return $query->leftJoin('rooms', 'hotel_guests.room_id', '=', 'rooms.id')
            ->where('rooms.hotel_id', auth()->user()->hotel_id)
            ->select('hotel_guests.*')
            ->paginate(setting('site.perpage'));
    }
}
