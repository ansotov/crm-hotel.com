<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

class HotelLanguage extends Model
{

    /**
     * Hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotels\Hotel', 'hotel_id', 'id');
    }

    /**
     * Language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function language()
    {
        return $this->belongsTo('App\Models\Language', 'lang_id', 'id');
    }
}
