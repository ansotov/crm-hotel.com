<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class HotelOrderStatus extends Model
{

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Hotels\HotelOrderStatusData', 'id', 'status_id')->where(['lang_id' => Auth::user()->lang_id]);
    }
}
