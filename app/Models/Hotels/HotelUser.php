<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

class HotelUser extends Model
{

    /**
     * User
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
