<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelService extends Model
{
    use SoftDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'hotel_id',
        'service_id',
        'type_id',
        'slug',
        'active',
    ];

    /**
     * Service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function service()
    {
        return $this->belongsTo('App\Models\Services\Service', 'service_id', 'id');
    }

    /**
     * Service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id', 'id');
    }

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Hotels\HotelServiceData', 'id', 'service_id')->where(['lang_id' => auth()->user()->lang_id]);
    }

    /**
     * Images
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\Hotels\HotelServiceImage', 'hotel_service_id', 'id');
    }

    /*
     * Filters
     */

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeHotelItems($query)
    {
        return $query->join('hotel_service_data', 'hotel_service_data.service_id', '=', 'hotel_services.id')
            ->select('hotel_services.*')
            ->where('hotel_service_data.lang_id', auth()->user()->lang_id)
            ->where('hotel_id', auth()->user()->hotel_id)
            ->paginate(setting('site.perpage'));
    }

    /**
     * Sorting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $field
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAsc($query, $field = 'title')
    {
        return $query->leftJoin('hotel_service_data', 'hotel_service_data.service_id', '=', 'hotel_services.id')
            ->orderBy('hotel_service_data.' . $field, 'asc');
    }
}
