<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageData extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql_geo';

    /**
     * @var array
     */
    protected $fillable = [
        'lang_id',
        'language_id',
        'title',
    ];
}
