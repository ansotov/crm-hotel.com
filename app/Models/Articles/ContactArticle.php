<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Model;

class ContactArticle extends Model
{

    /**
     * @var string[]
     */
    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data()
    {
        return $this->hasOne('App\Models\Articles\ContactArticleData', 'article_id', 'id')->where(['lang_id' => auth()->user()->lang_id]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelItem($query)
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }
}
