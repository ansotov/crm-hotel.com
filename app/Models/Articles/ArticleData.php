<?php

namespace App\Models\Articles;

use Illuminate\Database\Eloquent\Model;

class ArticleData extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'lang_id',
        'article_id',
        'title',
        'description',
        'text',
    ];
}
