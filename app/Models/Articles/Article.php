<?php

namespace App\Models\Articles;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $dates = ['published_at', 'deleted_at'];

    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Articles\ArticleData', 'id', 'article_id')->where(['lang_id' => auth()->user()->lang_id]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelArticles($query)
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }

    /**
     * @param $query
     * @param $slug
     *
     * @return mixed
     */
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Scope active offers by date and hotel
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where(
            [
                'archive' => false,
                'active'  => true,
            ]
        )
            ->where('published_at', '<=', Carbon::now()->toDateString());
    }
}
