<?php

namespace App\Models\Rooms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Room extends Model
{
    use SoftDeletes, Notifiable;

    protected $updateDirtyOnDelete = false;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => \App\Events\Hotels\Room::class,
    ];

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'hotel_id',
        'type_id',
        'number',
        'comment',
        'deleted',
        'active',
    ];

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function type()
    {
        return $this->belongsTo('App\Models\Rooms\RoomType', 'type_id', 'id');
    }

    /*
     * Filters
     */

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelRooms($query)
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }

    /**
     * Sorting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $field
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAsc($query, $field = 'number')
    {
        return $query->orderByRaw('LENGTH(' . $field . ') ASC')->orderBy($field, 'asc');
    }
}
