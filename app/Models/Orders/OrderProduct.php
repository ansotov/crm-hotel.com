<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    public $timestamps = false;

    /**
     * Order
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Orders\Order', 'id', 'order_id');
    }

    /**
     * Service
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function service()
    {
        return $this->belongsTo('App\Models\Hotels\HotelService', 'service_id', 'id');
    }

    /**
     * Currency
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id', 'id');
    }
}
