<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrderStatus extends Model
{

    /**
     * Data
     *
     * @param bool $langId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data($langId = false)
    {
        if ($langId) {
            return $this->belongsTo('App\Models\Orders\OrderStatusData', 'id', 'status_id')->where(['lang_id' => $langId])->first();
        } else {
            return $this->belongsTo('App\Models\Orders\OrderStatusData', 'id', 'status_id')->where(['lang_id' => Auth::user()->lang_id]);
        }
    }
}
