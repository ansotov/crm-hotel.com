<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $dates = ['published_at', 'deleted_at'];

    protected $fillable = [
        'slug',
        'hotel_id',
        'user_id',
        'status_id',
        'comment',
    ];

    /**
     * Products
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function products()
    {
        return $this->hasMany('App\Models\Orders\OrderProduct', 'order_id', 'id');
    }

    /**
     * Status
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Orders\OrderStatus', 'status_id', 'id');
    }

    /**
     * User
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id', 'user_id');
    }


    /**
     * Scope a query to only include hotel orders.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelOrders($query)
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }

    /**
     * User orders by hotel
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeHotelUser($query)
    {
        return $query->where(['hotel_id' => auth()->user()->hotel_id, 'user_id' => auth()->user()->id])->orderBy('created_at', 'desc');
    }
}
