<?php

namespace App\Models\Offers;

use Illuminate\Database\Eloquent\Model;

class OfferData extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'lang_id',
        'offer_id',
        'title',
        'description',
        'text',
    ];
}
