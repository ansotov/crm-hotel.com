<?php

namespace App\Models\Offers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use SoftDeletes;

    protected $dates = ['publish_at', 'deleted_at'];

    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Offers\OfferData', 'id', 'offer_id')->where(['lang_id' => auth()->user()->lang_id]);
    }

    /**
     * Images
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\Offers\OfferImage', 'offer_id', 'id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeHotelItems($query)
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }

    /**
     * @param $query
     * @param $slug
     *
     * @return mixed
     */
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * Scope active offers by date and hotel
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where(
            [
                'archive' => false,
                'active'  => true,
            ]
        )
            ->where('start_at', '<=', Carbon::now()->toDateTimeString())
            ->where('end_at', '>=', Carbon::now()->toDateTimeString());
    }
}
