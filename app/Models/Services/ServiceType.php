<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Services\ServiceTypeData', 'id', 'type_id')->where(['lang_id' => auth()->user()->lang_id]);
    }
}
