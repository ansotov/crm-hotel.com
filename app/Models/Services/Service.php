<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //use SoftDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function type()
    {
        return $this->belongsTo('App\Models\Services\ServiceType', 'type_id', 'id');
    }

    /*
     * Filters
     */

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Sorting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $field
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAscItems($query, $field = 'title')
    {
        return $query->leftJoin('service_data', 'service_data.service_id', '=', 'services.id')
            ->select('services.*')
            ->where('service_data.lang_id', auth()->user()->lang_id)
            ->orderBy('service_data.' . $field, 'asc')
            ->paginate(setting('site.perpage'));
    }

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\Services\ServiceData', 'id', 'service_id')->where(['lang_id' => auth()->user()->lang_id]);
    }
}
