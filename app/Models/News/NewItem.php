<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewItem extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    protected $dates = ['published_at', 'deleted_at'];

    protected $fillable = [
        'hotel_id',
        'slug',
        'active',
    ];

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author ansotov
     */
    public function data()
    {
        return $this->belongsTo('App\Models\News\NewData', 'id', 'new_id')->where(['lang_id' => auth()->user()->lang_id]);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHotelNews($query)
    {
        return $query->where('hotel_id', auth()->user()->hotel_id);
    }
}
