<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class NewData extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'lang_id',
        'new_id',
        'title',
        'description',
        'text',
    ];
}
