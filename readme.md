Hotel CRM system - user interface.

Init commands.

    cd root_project_directory
    sudo chmod -R 777 storage
    sudo chmod -R 777 bootstrap/cache
    composer install
    php artisan key:generate
    php artisan storage:link
    php -r "file_exists('.env') || copy('.env.example', '.env');"
    composer update
    composer dump-autoload
