<?php 
return array (
  'add' => 'Hinzufügen',
  'restore' => 'Wiederherstellen',
  'edit' => 'Bearbeiten',
  'delete' => 'Löschen',
  'reset' => 'Zurücksetzen',
  'save' => 'speichern',
  'search' => 'Suche',
  'search.placeholder' => 'Suchen nach...',
  'yes' => 'Ja',
  'no' => 'Nein',
  'send' => 'Senden',
  'details' => 'Einzelheiten',
);