<?php 
return array (
  'number' => 'Nummer',
  'type' => 'Art',
  'comment' => 'Kommentar',
  'active' => 'Aktiv',
  'name' => 'Name',
  'guest' => 'Gast',
  'start_at' => 'Check-In',
  'end_at' => 'Auschecken',
  'service' => 'Bedienung',
  'title' => 'Titel',
  'description' => 'Beschreibung',
  'text' => 'Text',
);