<?php

return [
    'home'      => 'Home',
    'main'      => 'Main',
    'profile'   => 'Profile',
    'orders'    => 'Orders',
    'rooms'     => 'Rooms',
    'guests'    => 'Guests',
    'services'  => 'Services',
    'offers'    => 'Offers',
    'news'      => 'News',
    'articles'  => 'Articles',
    'logout'    => 'Logout',
    'hotels'    => 'Hotels',
    'login'     => 'Login',
    'my-orders' => 'My orders',
    'contacts'  => 'Contacts',
];
