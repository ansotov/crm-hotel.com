<?php

return [
    'rooms'               => 'Rooms',
    'add'                 => 'add',
    'edit'                => 'edit',
    'hotel'               => 'Hotel',
    'guests'              => 'Guests',
    'services'            => 'Services',
    'offers'              => 'Offers',
    'news'                => 'News',
    'articles'            => 'Articles',
    'profile'             => 'Profile',
    'service-description' => 'Proposals of your hotel',
    'images'              => 'Images',
    'myOrders'            => 'My orders',
    'myOrdersDescription' => 'Previous orders',
    'offer-description'   => 'Offers of your hotel',
    'article-description' => 'Useful information about our hotel',
    'contacts'            => 'Hotel contacts',
    'contact-description' => 'Useful contacts of your hotel',
];
