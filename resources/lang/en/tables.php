<?php

return [
    'number'      => 'Number',
    'type'        => 'Type',
    'comment'     => 'Comment',
    'active'      => 'Active',
    'name'        => 'Name',
    'guest'       => 'Guest',
    'start_at'    => 'Check in',
    'end_at'      => 'Check out',
    'service'     => 'Service',
    'title'       => 'Title',
    'description' => 'Description',
    'text'        => 'Text',
];
