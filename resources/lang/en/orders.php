<?php

return [
    'name'    => 'Name',
    'price'   => 'Price',
    'qty'     => 'Quantity',
    'service' => 'Service',
    'comment' => 'Comment',
    'status'  => 'Status',
];
