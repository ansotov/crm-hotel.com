<?php

return [
    'add'                => 'Add',
    'restore'            => 'Restore',
    'edit'               => 'Edit',
    'delete'             => 'Delete',
    'reset'              => 'Reset',
    'save'               => 'Save',
    'search'             => 'Search',
    'search.placeholder' => 'Search for...',
    'yes'                => 'Yes',
    'no'                 => 'No',
    'send'               => 'Send',
    'details'            => 'Details',
];
