<?php 
return array (
  'home' => 'Domov',
  'main' => 'Hlavní',
  'profile' => 'Profil',
  'orders' => 'Objednávky',
  'rooms' => 'Pokoje',
  'guests' => 'Hosté',
  'services' => 'Služby',
  'offers' => 'Nabídky',
  'news' => 'Zprávy',
  'articles' => 'Články',
  'logout' => 'Odhlásit se',
  'hotels' => 'Hotely',
  'login' => 'Přihlásit se',
  'my-orders' => 'Mé objednávky',
  'contacts' => 'Kontakty',
);