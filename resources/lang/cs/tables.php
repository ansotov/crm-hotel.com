<?php 
return array (
  'number' => 'Číslo',
  'type' => 'Typ',
  'comment' => 'Komentář',
  'active' => 'Aktivní',
  'name' => 'název',
  'guest' => 'Host',
  'start_at' => 'Check in',
  'end_at' => 'Překontrolovat',
  'service' => 'Služba',
  'title' => 'Titul',
  'description' => 'Popis',
  'text' => 'Text',
);