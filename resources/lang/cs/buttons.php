<?php 
return array (
  'add' => 'Přidat',
  'restore' => 'Obnovit',
  'edit' => 'Upravit',
  'delete' => 'Odstranit',
  'reset' => 'Resetovat',
  'save' => 'Uložit',
  'search' => 'Vyhledávání',
  'search.placeholder' => 'Hledat...',
  'yes' => 'Ano',
  'no' => 'Ne',
  'send' => 'Poslat',
  'details' => 'Detaily',
);