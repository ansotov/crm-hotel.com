<?php 
return array (
  'number' => 'Номер',
  'type' => 'Тип',
  'comment' => 'Комментарий',
  'active' => 'Включено',
  'name' => 'Имя',
  'guest' => 'Гость',
  'start_at' => 'Заезд',
  'end_at' => 'Выезд',
  'service' => 'Сервис',
  'title' => 'Заголовок',
  'description' => 'Описание',
  'text' => 'Текст',
);