<?php 
return array (
  'home' => 'Главная',
  'main' => 'Основной',
  'profile' => 'Профиль',
  'orders' => 'Заказы',
  'rooms' => 'Номера',
  'guests' => 'Гости',
  'services' => 'Услуги',
  'offers' => 'Акции',
  'news' => 'Новости',
  'articles' => 'Информация',
  'logout' => 'Выйти',
  'hotels' => 'Отели',
  'login' => 'Авторизоваться',
  'my-orders' => 'Мои заказы',
  'contacts' => 'Контакты',
);