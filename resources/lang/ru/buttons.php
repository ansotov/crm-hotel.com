<?php 
return array (
  'add' => 'Добавить',
  'restore' => 'Восстановить',
  'edit' => 'Редактировать',
  'delete' => 'удалять',
  'reset' => 'Сброс настроек',
  'save' => 'Сохранить',
  'search' => 'Поиск',
  'search.placeholder' => 'Искать...',
  'yes' => 'да',
  'no' => 'нет',
  'send' => 'послать',
  'details' => 'подробности',
);