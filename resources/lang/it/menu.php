<?php 
return array (
  'home' => 'Casa',
  'main' => 'Principale',
  'profile' => 'Profilo',
  'orders' => 'Ordini',
  'rooms' => 'Camere',
  'guests' => 'Ospiti',
  'services' => 'Servizi',
  'offers' => 'Offerte',
  'news' => 'notizia',
  'articles' => 'Artificio',
  'logout' => 'Disconnettersi',
  'hotels' => 'Hotel',
  'login' => 'Login',
  'my-orders' => 'I miei ordini',
  'contacts' => 'Contatti',
);