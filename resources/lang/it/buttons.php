<?php 
return array (
  'add' => 'Inserisci',
  'restore' => 'Ristabilire',
  'edit' => 'modificare',
  'delete' => 'Elimina',
  'reset' => 'Ripristina',
  'save' => 'Salva',
  'search' => 'Ricerca',
  'search.placeholder' => 'Cercare...',
  'yes' => 'sì',
  'no' => 'No',
  'send' => 'Spedire',
  'details' => 'Dettagli',
);