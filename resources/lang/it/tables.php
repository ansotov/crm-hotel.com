<?php 
return array (
  'number' => 'Numero',
  'type' => 'genere',
  'comment' => 'Commento',
  'active' => 'Attivo',
  'name' => 'Nome',
  'guest' => 'ospite',
  'start_at' => 'Registrare',
  'end_at' => 'Check-out',
  'service' => 'Servizio',
  'title' => 'Titolo',
  'description' => 'Descrizione',
  'text' => 'Testo',
);