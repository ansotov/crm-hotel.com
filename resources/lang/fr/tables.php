<?php 
return array (
  'number' => 'Nombre',
  'type' => 'Taper',
  'comment' => 'Commenter',
  'active' => 'actif',
  'name' => 'Nom',
  'guest' => 'Invité',
  'start_at' => 'Enregistrement',
  'end_at' => 'Vérifier',
  'service' => 'Un service',
  'title' => 'Titre',
  'description' => 'La description',
  'text' => 'Texte',
);