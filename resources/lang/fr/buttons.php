<?php 
return array (
  'add' => 'Ajouter',
  'restore' => 'Restaurer',
  'edit' => 'Éditer',
  'delete' => 'Effacer',
  'reset' => 'Réinitialiser',
  'save' => 'Sauvegarder',
  'search' => 'Rechercher',
  'search.placeholder' => 'Rechercher...',
  'yes' => 'Oui',
  'no' => 'Non',
  'send' => 'Envoyer',
  'details' => 'Des détails',
);