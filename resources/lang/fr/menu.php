<?php 
return array (
  'home' => 'Domicile',
  'main' => 'Principale',
  'profile' => 'Profil',
  'orders' => 'Ordres',
  'rooms' => 'Pièces',
  'guests' => 'Invités',
  'services' => 'Prestations de service',
  'offers' => 'Des offres',
  'news' => 'Nouvelles',
  'articles' => 'Des articles',
  'logout' => 'Se déconnecter',
  'hotels' => 'Hôtels',
  'login' => 'Connexion',
  'my-orders' => 'Mes commandes',
  'contacts' => 'Contacts',
);