<?php 
return array (
  'rooms' => 'Pièces',
  'add' => 'ajouter',
  'edit' => 'Éditer',
  'hotel' => 'Hôtel',
  'guests' => 'Invités',
  'services' => 'Prestations de service',
  'offers' => 'Des offres',
  'news' => 'Nouvelles',
  'articles' => 'Des articles',
  'profile' => 'Profil',
  'service-description' => 'Propositions de votre hôtel',
  'images' => 'Images',
  'myOrders' => 'Mes commandes',
  'myOrdersDescription' => 'Commandes précédentes',
  'offer-description' => 'Offres de votre hôtel',
  'article-description' => 'Informations utiles sur notre hôtel',
  'contacts' => 'Contacts de l\'hôtel',
  'contact-description' => 'Contacts utiles de votre hôtel',
);