@extends('layouts.app')

@section('title')
    {{ __('titles.profile') }}
@endsection

@section('content')
    <!--/ Services Star /-->
    <section>
        <div class="container">
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_content">
                        {!! Form::open(['method' => 'post', 'url' => route('profile.update')]) !!}
                        @method('PATCH')

                        <div class="form-group col-md-8">
                            {!! Form::label('lang_id', __('forms.language')) !!}
                            <select name="lang_id" class="form-control">
                                @foreach($item->languages as $k => $v)
                                    <option
                                        value="{{ $v->language->id }}"{{ $v->language->id == $item->lang_id ? ' selected' : false }}>{{ $v->language->data->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-8">
                            {!! Form::label('name', __('forms.name')) !!}
                            {!! Form::text('name', $item->name, ['class' => 'form-control']) !!}
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-8">
                            {!! Form::label('email', __('forms.email')) !!}
                            {!! Form::email('email', $item->email, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-md-12">
                            {{ Form::submit(__('buttons.save'), ['class' => 'btn btn-primary']) }}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="x_panel">
                    <div class="x_content">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Services End /-->
@endsection
