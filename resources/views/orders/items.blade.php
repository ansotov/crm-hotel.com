@extends('layouts.app')

@section('title')
    {{ __('titles.myOrders') }}
@endsection

@section('content')
    <!--/ Intro Single star /-->
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="title-single-box">
                        <h1 class="title-single">{{ __('titles.myOrders') }}</h1>
                        <span class="color-text-a">{{ __('titles.myOrdersDescription') }}</span>
                    </div>
                </div>
                {{--<div class="col-md-12 col-lg-4">
                    {{ Breadcrumbs::render('orders') }}
                </div>--}}
            </div>
        </div>
    </section>
    <!--/ Intro Single End /-->

    <!--/ Property Grid Star /-->
    <section class="property-grid grid">
        <div class="container">
            <div class="row">
                @foreach($items as $k => $v)
                    <div class="col-md-4">
                        <div class="card-box-c foo" data-sr-id="1" style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.015s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.015s; transition: transform 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.015s, opacity 1s cubic-bezier(0.6, 0.2, 0.1, 1) 0.015s; ">
                            <div>
                                <div class="card-title-c align-self-center">
                                    <h2><b>№</b>: {{ $v->slug }}</h2>

                                    <b>{{ __('orders.status') }}</b>: {{ $v->status->data->title }}
                                </div>
                            </div>
                            <div>
                                <p class="content-c">
                                    @foreach($v->products as $k2 => $v2)
                                        <ul class="list-unstyled">
                                            <li><b>{{ __('orders.service') }}</b>: <a href="{{ route('services.item', ['slug' => $v2->service->slug]) }}">{{ $v2->service->data->title }}</a></li>
                                            <li><b>{{ __('orders.price') }}</b>: {{ $v2->price }}{{ $v2->currency->symbol }}
                                                ({{ $v2->currency->currency }})
                                            </li>
                                            @if($v2->qty > 1)
                                                <li><b>{{ __('orders.qty') }}</b>: {{ $v2->qty }}</li>
                                            @endif
                                            @if($v2->comment)
                                                <li><b>{{ __('orders.comment') }}</b>: {{ $v2->comment }}</li>
                                            @endif
                                        </ul>
                                        <hr>
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $items->links() }}
        </div>
    </section>
    <!--/ Property Grid End /-->
@endsection
