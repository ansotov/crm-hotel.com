<ul class="list-inline menu-block">
    <li class="nav-item dropdown">
        <a
            class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            {{ $currentLang->data->title}}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            @foreach($items as $k => $v)
                <a class="dropdown-item" href="{{ route('lang', ['lang' => $v->const]) }}">{{ $v->data->title }}</a>
            @endforeach
        </div>
    </li>
</ul>
