@extends('layouts.app')

@section('title')
    {{ __('titles.profile') }}
@endsection

@section('content')

    <!--/ Intro Single star /-->
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="title-single-box">
                        <h1 class="title-single">{{ $item->data->title }}</h1>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    {{ Breadcrumbs::render('services.item', $item) }}
                </div>
            </div>
        </div>
    </section>
    <!--/ Intro Single End /-->

    <!--/ Property Single Star /-->
    <section class="property-single nav-arrow-b">
        <div class="container">
            <div class="row">
                @if($item->images->count() > 0)
                    <div class="col-md-12 col-lg-4">
                        <div
                            id="property-single-carousel" class="owl-carousel owl-arrow gallery-property">
                            @foreach($item->images as $k => $v)
                                <div class="carousel-item-b">
                                    <img
                                        src="{{ frontHotelImages($v->image, 'services', 'm') }}"
                                        alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8">
                        <div class="title-single-box">
                            <div class="property-description">
                                <div class="property-price d-flex justify-content-center foo" style="float: left; padding: 0 10px 0 0">
                                    <div class="card-header-c d-flex">
                                        <div class="card-box-ico">
                                            <span class="ion-money">{{ $item->currency->symbol }}</span>
                                        </div>
                                        <div class="card-title-c align-self-center">
                                            <h5 class="title-c">{{ $item->price }}</h5>
                                        </div>
                                    </div>
                                </div>
                                {!! $item->data->description !!}
                            </div>
                            <div class="label-description">
                                <p class="mb-4">{{ __('forms.order-label') }}</p>
                            </div>
                            <div class="property-contact">
                                <form class="form-a" method="post" action="{{ route('orders.create') }}">
                                    @csrf
                                    <input type="hidden" name="service_id" value="{{ $item->id }}">
                                    <div class="row">
                                        <div class="col-md-12 mb-1">
                                            <div class="form-group">
                                                <textarea
                                                    id="textMessage" class="form-control"
                                                    placeholder="{{ __('forms.comment') }}"
                                                    name="comment" cols="45"
                                                    rows="6" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button
                                                type="submit" class="btn btn-a">{{ __('forms.send-order') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="divider" style="height: 40px"></div>
                        </div>
                    </div>
                @else
                    <div class="col-md-12 col-lg-12">
                        <div class="title-single-box">
                            <div class="property-description">
                                <div class="property-price d-flex justify-content-center foo" style="float: left; padding: 0 10px 0 0">
                                    <div class="card-header-c d-flex">
                                        <div class="card-box-ico">
                                            <span class="ion-money">{{ $item->currency->symbol }}</span>
                                        </div>
                                        <div class="card-title-c align-self-center">
                                            <h5 class="title-c">{{ $item->price }}</h5>
                                        </div>
                                    </div>
                                </div>
                                {!! $item->data->description !!}
                            </div>
                            <div style="clear: both"></div>
                            <br>
                            <div class="label-description">
                                <p class="mb-4">{{ __('forms.order-label') }}</p>
                            </div>
                            <div class="property-contact">
                                <form class="form-a" method="post" action="{{ route('orders.create') }}">
                                    @csrf
                                    <input type="hidden" name="service_id" value="{{ $item->id }}">
                                    <div class="row">
                                        <div class="col-md-12 mb-1">
                                            <div class="form-group">
                                                <textarea
                                                    id="textMessage" class="form-control"
                                                    placeholder="{{ __('forms.comment') }}"
                                                    name="comment" cols="45"
                                                    rows="6" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button
                                                type="submit" class="btn btn-a">{{ __('forms.send-order') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="divider" style="height: 40px"></div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
