@extends('layouts.app')

@section('title')
    {{ __('titles.services') }}
@endsection

@section('content')
    <!--/ Intro Single star /-->
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="title-single-box">
                        <h1 class="title-single">{{ __('titles.offers') }}</h1>
                        <span class="color-text-a">{{ __('titles.offer-description') }}</span>
                    </div>
                </div>
                {{--<div class="col-md-12 col-lg-4">
                    {{ Breadcrumbs::render('services') }}
                </div>--}}
            </div>
        </div>
    </section>
    <!--/ Intro Single End /-->

    <!--/ Property Grid Star /-->
    <section class="property-grid grid">
        <div class="container">
            <div class="row">
                @foreach($items as $k => $v)
                    <div class="col-md-4">
                        <div class="card-box-a">
                            <div class="img-box-a">
                                <div style="background-image: url('{{ offerImage($v->id) }}') repeat;min-height: 300px"
                                     class="img-a img-fluid"></div>
                            </div>
                            <div class="card-overlay">
                                <div class="">
                                    <div class="card-header-a">
                                        <h2>
                                            <a href="{{ route('offers.item', ['slug' => $v->slug]) }}">{{ $v->data->title }}</a>
                                        </h2>
                                        <div>{!! \Illuminate\Support\Str::limit($v->data->description, 300) !!}</div>
                                        <a href="{{ route('offers.item', ['slug' => $v->slug]) }}">
                                            {{ __('buttons.details') }}
                                            <span class="ion-ios-arrow-forward"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $items->links() }}
        </div>
    </section>
    <!--/ Property Grid End /-->
@endsection
