@extends('layouts.app')

@section('title')
    {{ __('titles.profile') }}
@endsection

@section('content')

    <!--/ Intro Single star /-->
    <section class="intro-single">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="title-single-box">
                        <h1 class="title-single">{{ $item->data->title }}</h1>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    {{ Breadcrumbs::render('offers.item', $item) }}
                </div>
            </div>
        </div>
    </section>
    <!--/ Intro Single End /-->

    <!--/ Property Single Star /-->
    <section class="property-single nav-arrow-b">
        <div class="container">
            <div class="row">
                @if($item->images->count() > 0)
                    <div class="col-md-12 col-lg-4">
                        <div
                            id="property-single-carousel" class="owl-carousel owl-arrow gallery-property">
                            @foreach($item->images as $k => $v)
                                <div class="carousel-item-b">
                                    <img
                                        src="{{ frontHotelImages($v->image, 'offers', 'm') }}"
                                        alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8">
                        <div class="title-single-box">
                            <div class="property-description">
                                {!! $item->data->description !!}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-12 col-lg-12">
                        <div class="title-single-box">
                            <div class="property-description">
                                {!! $item->data->description !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
