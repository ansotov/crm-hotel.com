<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Favicons -->
    <link href="{{ asset('front/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('front/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('front/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{ asset('front/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{ asset('front/css/app.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/jquery-confirm.min.css') }}" rel="stylesheet">
</head>


<body>

<div class="click-closed"></div>
<!--/ Form window /-->
<div class="box-collapse" id="window-wrapper"></div>
<!--/ Form window End /-->

<!--/ Nav Star /-->
<nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
        <button
            class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
            aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <a class="navbar-brand text-brand" href="{{ route('main') }}">My
            <span class="color-b">hotel</span>
        </a>
        <button
            type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
            data-target="#navbarTogglerDemo01" aria-expanded="false">
            <span class="fa fa-login" aria-hidden="true"></span>
        </button>

        @if (Auth::check())
            <div class="navbar-collapse collapse justify-content-center top-menu" id="navbarDefault">
                <ul class="navbar-nav">
                    {{ menu('main.top', null, ['class' => 'nav-item', 'li_a_class' => 'nav-item-link']) }}
                </ul>
            </div>
            @widget('languages')
        @endif
        {{--<button
            style="margin: 0 10px 0 0"
            type="button" class="btn btn-b-n popup-ajax-link" data-toggle="collapse"
            data-target="#navbarTogglerDemo01" aria-expanded="false">
            <span class="fa fa-search" aria-hidden="true"></span>
        </button>--}}
        <ul class="list-inline menu-block">
            {{--<li class="list-inline-item" style="margin: 0 30px 0 0">
                <a
                    href="javascript:void(0);" class="navbar-toggle-box-collapse"
                    data-toggle="collapse">
                    <span class="fa fa-search" aria-hidden="true"></span>
                </a>
            </li>--}}

            @if (Auth::check())
                {{--<li class="list-inline-item">
                    <a
                        href="{{ route('profile.edit') }}">
                        <span class="fa fa-user" aria-hidden="true"></span>
                    </a>
                </li>--}}
                <li class="list-inline-item">
                    <a
                        href="{{ route('logout') }}"
                        class="logout"
                        data-toggle="collapse">
                        <span class="fa fa-sign-out" aria-hidden="true"></span>
                    </a>
                    <form
                        id="logout-form" action="{{ route('logout') }}" method="POST"
                        style="display: none;">
                        @csrf
                    </form>
                </li>
            @else
                <li class="list-inline-item">
                    <form method="POST" action="{{ route('auth.code') }}" class="form-a ajax-send">
                        @csrf
                        <button type="submit" class="btn" style="float: right">></button>
                        <input autofocus class="auth-code" required minlength="6" type="text" name="code" placeholder="{{ __('auth.insert-code') }}">
                    </form>
                </li>
                {{--<li class="list-inline-item">
                    <a
                        href="javascript:void(0);" class="navbar-toggle-box-collapse"
                        data-toggle="collapse"
                        onclick="dataToWindow('{{ route('register') }}')">
                        <span class="fa fa-user-o" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a
                        href="javascript:void(0);" class="navbar-toggle-box-collapse"
                        data-toggle="collapse"
                        onclick="dataToWindow('{{ route('login') }}')">
                        <span class="fa fa-sign-in" aria-hidden="true"></span>
                    </a>
                </li>--}}
            @endif
        </ul>
    </div>
</nav>
<!--/ Nav End /-->

<div class="section-services section-t8"></div>

@yield('content')

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="nav-footer">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">About</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">Property</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">Blog</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">Contact</a>
                        </li>
                    </ul>
                </nav>
                <div class="socials-a">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-dribbble" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="copyright-footer">
                    <p class="copyright color-text-a">
                        {{ __('main.copyright') }}
                        @
                        <a href="https://site-town.com" target="_blank">site-town.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ Footer End /-->

<a href="#" class="back-to-top">
    <i class="fa fa-chevron-up"></i>
</a>
<div id="preloader"></div>

<!-- JavaScript Libraries -->
<script src="{{ asset('front/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('front/lib/jquery/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('front/lib/popper/popper.min.js') }}"></script>
<script src="{{ asset('front/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('front/lib/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('front/lib/scrollreveal/scrollreveal.min.js') }}"></script>
<script src="{{ asset('front/js/jquery-confirm.min.js') }}"></script>
<!-- Contact Form JavaScript File -->
<script src="{{ asset('front/contactform/contactform.js') }}"></script>

<!-- Template Main Javascript File -->
<script type="text/javascript" src="{{ asset('front/js/functions.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/app.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front/js/bootstrap-notify.min.js') }}"></script>

@yield('footer_scripts')

{{-- Notifications --}}
@if (session('success'))
    <script type="text/javascript">
        $.notify({
            message: '{{ session('success') }}'
        }, {
            type: 'success',
            placement: {
                from: "top",
                align: "right"
            }
        });
    </script>
@endif

@if (session('status'))
    <script type="text/javascript">
        $.notify({
            message: '{{ session('status') }}'
        }, {
            type: 'info',
            placement: {
                from: "top",
                align: "right"
            }
        });
    </script>
@endif

@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script type="text/javascript">
            $.notify({
                message: '{{ $error }}'
            }, {
                type: 'danger',
                className: 'notifications'
            });
        </script>
    @endforeach
@endif

</body>
</html>
