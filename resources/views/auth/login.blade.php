<div class="title-box-d">
    <h3 class="title-d">{{ __('forms.login') }}</h3>
</div>
<span class="close-box-collapse right-boxed ion-ios-close"></span>
<div class="box-collapse-wrap ajax-data">

    <div class="formErrors"></div>

    <form method="POST" action="{{ route('login') }}" class="form-a ajax-send">
        @csrf
        <div class="row">
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="email">{{ __('forms.email') }}</label>
                    <input
                        id="email" type="email"
                        class="form-control form-control-lg form-control-a @error('email') is-invalid @enderror"
                        name="email"
                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="password">{{ __('forms.password') }}</label>
                    <input
                        id="password" type="password"
                        class="form-control form-control-lg form-control-a @error('password') is-invalid @enderror"
                        name="password"
                        value="" required autocomplete="password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="remember">
                        <input
                            type="checkbox" name="remember"
                            id="remember" {{ old('remember') ? 'checked' : '' }}>&nbsp;

                        {{ __('forms.remember') }}</label>
                    @error('remember')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-b">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" onclick="dataToWindow('{{ route('password.request') }}')" href="javascript:void(0);">
                            {{ __('forms.forgot-password') }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>
