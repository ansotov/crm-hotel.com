<div class="title-box-d">
    <h3 class="title-d">{{ __('forms.registration') }}</h3>
</div>
<span class="close-box-collapse right-boxed ion-ios-close"></span>
<div class="box-collapse-wrap ajax-data">
    <form method="POST" action="{{ route('register') }}" class="form-a ajax-send">
        @csrf
        <div class="row">
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="name">{{ __('forms.name') }}</label>
                    <input
                        id="name" type="text"
                        class="form-control form-control-lg form-control-a @error('name') is-invalid @enderror"
                        name="name"
                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="email">{{ __('forms.email') }}</label>
                    <input
                        id="email" type="email"
                        class="form-control form-control-lg form-control-a @error('email') is-invalid @enderror"
                        name="email"
                        value="{{ old('email') }}" required autocomplete="name" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="password">{{ __('forms.password') }}</label>
                    <input
                        id="password" type="password"
                        class="form-control form-control-lg form-control-a @error('password') is-invalid @enderror"
                        name="password"
                        value="{{ old('password') }}" required>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="password_confirmation">{{ __('forms.password_confirmation') }}</label>
                    <input
                        id="password_confirmation" type="password"
                        class="form-control form-control-lg form-control-a @error('password_confirmation') is-invalid @enderror"
                        name="password_confirmation"
                        value="{{ old('password_confirmation') }}" required>

                    @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-b">
                        {{ __('forms.registration') }}
                    </button>

                    <a
                        class="btn btn-link" onclick="dataToWindow('{{ route('login') }}')"
                        href="javascript:void(0);">
                        {{ __('forms.login') }}
                    </a>
                </div>
            </div>
        </div>
    </form>
</div>
