<div class="title-box-d">
    <h3 class="title-d">{{ __('forms.reset_password') }}</h3>
</div>
<span class="close-box-collapse right-boxed ion-ios-close"></span>
<div class="box-collapse-wrap ajax-data">

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}" class="form-a ajax-send">
        @csrf
        <div class="row">

            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <label for="email">{{ __('forms.email') }}</label>
                    <input
                        id="email" type="email"
                        class="form-control form-control-lg form-control-a @error('email') is-invalid @enderror"
                        name="email"
                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-12 mb-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-b">
                        {{ __('buttons.send') }}
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
